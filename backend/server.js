import express from "express";
import cors from "cors";
import CacheService from "express-api-cache";
import timeout from "connect-timeout";
import { createClient } from "redis";
import config from "./config.js";
import {
  getBorrowableMimsArbitrum,
  getBorrowableMimsAvalanche,
  getBorrowableMimsBinance,
  getBorrowableMimsEthereum,
  getBorrowableMimsFantom,
} from "./pools/borrowableMims.js";
import moment from "moment";

const PORT = 5001;
const app = express();
const cache = CacheService.cache;

app.use(cors());
app.use(timeout(600000));

let redis = null;

(async () => {
  if (config.env !== "dev") {
    redis = createClient();

    redis.on("error", (err) => console.log("Redis Client Error", err));

    await redis.connect();
  }
})();

const corsOptions = {
  origin:
    config.env === "dev"
      ? "http://localhost:3001"
      : "https://www.byebyedai.money",
};

app.get(
  "/getSpellHolders",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    console.log("fetching /getSpellHolders");
    const holdersByChain = await redis.get("byebyedai.spellHoldersArray");
    console.log("Done");
    res.json(JSON.parse(holdersByChain));
  }
);

app.get(
  "/getsSpellHolders",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    console.log("fetching /getsSpellHolders");
    const holdersByChain = await redis.get("byebyedai.stakedSpellHoldersArray");
    console.log("Done");
    res.json(JSON.parse(holdersByChain));
  }
);

app.get(
  "/getMkrHolders",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    console.log("fetching /getMkrHolders");
    const holdersByChain = await redis.get("byebyedai.mkrHoldersArray");
    console.log("Done");
    res.json(JSON.parse(holdersByChain));
  }
);

app.get(
  "/getMkrFees",
  cache("4 hours"),
  cors(corsOptions),
  async (req, res) => {
    const makerFees = await redis.get("byebyedai.makerFees");
    res.json(JSON.parse(makerFees));
  }
);

app.get(
  "/getSpellFees",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    const total = await redis.get("byebyedai.totalSpellFees");
    res.json(JSON.parse(total));
  }
);

app.get(
  "/getSpellCauldrons",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    const cauldronsArray = await redis.get("byebyedai.cauldronsArray");
    res.send({
      cauldrons: JSON.parse(cauldronsArray),
    });
  }
);

app.get(
  "/getHistoricalStakedSpellRatio",
  cache("30 seconds"),
  cors(corsOptions),
  async (req, res) => {
    const stakedRatioHistory = await redis.get(
      "byebyedai.stakedRatio.stakedRatioHistory"
    );
    res.send({
      stakedRatioHistory: JSON.parse(stakedRatioHistory),
    });
  }
);

app.get(
  "/getEmissions",
  cache("2 hours"),
  cors(corsOptions),
  async (req, res) => {
    const emissions = await redis.get(
      "byebyedai.totalEmissionsLastSevenDaysUSD"
    );
    res.json(JSON.parse(emissions));
  }
);

app.get(
  "/getBorrowableMims",
  cors(corsOptions),
  cache("5 seconds"),
  async (req, res) => {
    console.log("Update borrowable MIMs...", moment().format("HH:mm:ss"));
    const borrwableEth = await getBorrowableMimsEthereum();
    const borrowableAvax = await getBorrowableMimsAvalanche();
    const borrowableFtm = await getBorrowableMimsFantom();
    const borrowableArbi = await getBorrowableMimsArbitrum();
    const borrowableBsc = await getBorrowableMimsBinance();
    console.log("Update done.", moment().format("HH:mm:ss"));
    res.json({
      cauldrons: [
        borrwableEth,
        borrowableAvax,
        borrowableFtm,
        borrowableArbi,
        borrowableBsc,
      ],
    });
  }
);

app.listen(PORT, "127.0.0.1", () => {
  console.log(`Server listening for ${corsOptions.origin} requests ...`);
});
