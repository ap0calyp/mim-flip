export const AddressesByChain = [
  {
    chainId: 1,
    chainName: "Ethereum",
    spellAddress: "0x090185f2135308bad17527004364ebcc2d37e5f6",
    sSpellAddress: "0x26fa3fffb6efe8c1e69103acb4044c26b9a106a9",
    mkrAddress: "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2",
  },
  {
    chainId: 250,
    chainName: "Fantom",
    spellAddress: "0x468003b688943977e6130f4f68f23aad939a1040",
    sSpellAddress: null,
    mkrAddress: null,
  },
  {
    chainId: 42161,
    chainName: "Arbitrum",
    spellAddress: "0x3e6648c5a70a150a88bce65f4ad4d506fe15d2af",
    sSpellAddress: null,
    mkrAddress: null,
  },
  {
    chainId: 43114,
    chainName: "Avalanche",
    spellAddress: "0xCE1bFFBD5374Dac86a2893119683F4911a2F7814",
    sSpellAddress: null,
    mkrAddress: null,
  },
];
