/* eslint-disable no-undef */
export default {
  coingeckoApiURL: "https://api.coingecko.com/api/v3",
  backend:
    process.env.NODE_ENV === "production"
      ? "https://www.byebyedai.money/api"
      : "http://localhost:5001",
};
