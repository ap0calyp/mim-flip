import "../styles/ProgressBar.css";
import PropTypes from "prop-types";
import colors from "../styles/colors";
import { Flames } from "./Flames";
import { useEffect, useState } from "react";

const ProgressBar = ({ value }) => {
  let resizeCallToClear = null;
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth,
        }),
        100
      );
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const { width } = dimensions;
  const finalValue = parseInt(value) >= 100 ? 100 : value;
  const minProgressBarValue = value > 0 ? 3 : 0;
  let radius = 15;
  if (value < 10) {
    if (width <= 800) {
      radius = 5;
    }
  }
  const containerStyles = {
    height: 30,
    width: "100%",
    backgroundColor: "#e0e0de",
    borderRadius: radius,
  };

  const percentage = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.primary,
  };

  const fillerStyles = {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    height: "100%",
    width:
      finalValue < minProgressBarValue
        ? `${minProgressBarValue}%`
        : `${finalValue}%`,
    backgroundColor: colors.secondary,
    borderRadius: "inherit",
  };

  const labelStyles = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  };

  return (
    <div className="Progress-Bar-container">
      <span style={percentage}>{`${value}%`}</span>
      <div style={containerStyles}>
        <div style={fillerStyles}>
          <span style={labelStyles}></span>
          {value >= 100 && <Flames />}
        </div>
      </div>
    </div>
  );
};

ProgressBar.propTypes = {
  value: PropTypes.string,
};

export default ProgressBar;
