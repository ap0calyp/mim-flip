import "../../styles/Header.css";
import { Link } from "react-router-dom";

const Header = () => (
  <nav className="App-header">
    <div className="App-header-title-container">
      <h3>ByeByeDai</h3>
    </div>
    <div className="App-header-content-container">
      <Link className="App-header-button" to="/">
        Flippening
      </Link>
      <Link className="App-header-button" to="/stats">
        Stats
      </Link>
      <Link className="App-header-button" to="/mim">
        MIM
      </Link>
      <Link className="App-header-button" to="/about">
        About
      </Link>
    </div>
  </nav>
);

Header.propTypes = {};

export default Header;
