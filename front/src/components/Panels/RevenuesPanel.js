import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import ProgressBar from "./../ProgressBar";
import SpellLogo from "../../images/Spell-Background-Large.png";
import sSpellLogo from "../../images/sSpell-Background.png";
import { getRatio } from "../../utils/helpers";

const EmissionsPanel = ({ emissionsUSD, revenuesUSD }) => {
  const spellToMakerRatio = getRatio(revenuesUSD, emissionsUSD);

  return (
    <div className="Flip-panel-mcap-container">
      <div className="Flip-panel-item">
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"sSPELL earnings last 7 days"}</h4>
          <img src={sSpellLogo} alt="sSpell Logo" width={50} height={50} />
          <NumberFormat
            prefix="$"
            value={revenuesUSD}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
        <ProgressBar value={`${spellToMakerRatio.toFixed(2)}`} />
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"SPELL emissions last 7 days"}</h4>
          <img src={SpellLogo} alt="Spell Logo" width={50} height={50} />
          <NumberFormat
            prefix="$"
            value={emissionsUSD}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
      </div>
      {revenuesUSD > emissionsUSD && (
        <>
          <p className="Flip-panel-future-price-container">
            {"Last 7 days net Abracadabra revenues were"}
          </p>
          <p className="Flip-panel-future-price-container">
            <NumberFormat
              thousandSeparator
              suffix="$"
              value={revenuesUSD - emissionsUSD}
              displayType="text"
              renderText={(value) => value}
            />
          </p>
        </>
      )}
    </div>
  );
};

EmissionsPanel.propTypes = {
  emissionsUSD: PropTypes.number,
  revenuesUSD: PropTypes.number,
  SPELLPrice: PropTypes.number,
};

export default EmissionsPanel;
