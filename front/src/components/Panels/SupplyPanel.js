import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import ProgressBar from "./../ProgressBar";
import MIMLogo from "../../images/MIM-Logo.png";
import DAILogo from "../../images/DAI-Logo.png";
import USDCLogo from "../../images/USDC-Logo.png";
import USDTLogo from "../../images/USDT-Logo.png";
import Dragon from "../../images/Dragon.png";
import Skull from "../../images/Light-Skull.png";
import { getRatio } from "../../utils/helpers";

const SupplyPanel = ({ MIMCap, DAICap, USDCCap, USDTCap }) => {
  const formattedMIMCap = `${MIMCap}`.split(".")[0];
  const formattedDAICap = `${DAICap}`.split(".")[0];
  const formattedUSDCCap = `${USDCCap}`.split(".")[0];
  const formattedUSDTCap = `${USDTCap}`.split(".")[0];
  const mimToDaiRatio = getRatio(MIMCap, DAICap);
  const mimToUsdcRatio = getRatio(MIMCap, USDCCap);
  const mimToUsdtRatio = getRatio(MIMCap, USDTCap);
  return (
    <div className="Flip-panel-multi-items">
      <div className="Flip-panel-item">
        <div className="Flip-panel-text-container">
          <img src={Dragon} alt="Dragon Logo" width={75} height={50} />
        </div>
        <ProgressBar value={`${mimToDaiRatio.toFixed(2)}`} />
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"DAI Supply"}</h4>
          <img src={DAILogo} alt="DAI Logo" width={50} height={50} />
          <NumberFormat
            value={formattedDAICap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
      </div>
      <div className="Flip-panel-item">
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"MIM Supply"}</h4>
          <img src={MIMLogo} alt="MIM Logo" width={50} height={50} />
          <NumberFormat
            value={formattedMIMCap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
        <ProgressBar value={`${mimToUsdcRatio.toFixed(2)}`} />
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"USDC Supply"}</h4>
          <img src={USDCLogo} alt="USDC Logo" width={50} height={50} />
          <NumberFormat
            value={formattedUSDCCap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
      </div>
      <div className="Flip-panel-item">
        <div className="Flip-panel-text-container">
          <img src={Skull} alt="Skull Logo" width={50} height={50} />
        </div>
        <ProgressBar value={`${mimToUsdtRatio.toFixed(2)}`} />
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"USDT Supply"}</h4>
          <img src={USDTLogo} alt="USDT Logo" width={50} height={50} />
          <NumberFormat
            value={formattedUSDTCap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
      </div>
    </div>
  );
};

SupplyPanel.propTypes = {
  DAICap: PropTypes.number,
  MIMCap: PropTypes.number,
  USDCCap: PropTypes.number,
  USDTCap: PropTypes.number,
};

export default SupplyPanel;
