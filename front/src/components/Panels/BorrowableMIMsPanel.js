import "../../styles/MIMsPanel.css";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";

const BorrowableMIMsPanel = ({ chain, cauldrons }) => {
  const orderedCauldrons = cauldrons.sort((a, b) => a.name - b.name);
  console.log();
  return (
    <div className="MIM-Panel-container">
      <h3 className="MIM-Panel-chain">{chain}</h3>
      <div className="MIM-Cauldrons-container">
        <div className="MIM-Cauldrons-header">
          <div className="MIM-Cauldrons-header-name">Cauldron</div>
          <div className="MIM-Cauldrons-header-sub">MIMs left to borrow</div>
        </div>
        {orderedCauldrons.map((cauldron) => (
          <div key={cauldron.name} className="MIM-Cauldrons-row">
            <div className="MIM-Cauldrons-name">{cauldron.name}</div>
            <div className="MIM-Cauldrons-amount">
              <NumberFormat
                value={cauldron.borrowableMIMs}
                displayType="text"
                thousandSeparator
                renderText={(value) => (
                  <p className="Flip-panel-label">{value}</p>
                )}
              />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

BorrowableMIMsPanel.propTypes = {
  chain: PropTypes.string,
  cauldrons: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      borrowableMIMs: PropTypes.number,
    })
  ),
};

export default BorrowableMIMsPanel;
