import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import ReactTooltip from "react-tooltip";
import { BsQuestionOctagonFill } from "react-icons/bs";
import ProgressBar from "./../ProgressBar";
import sSpellLogo from "../../images/sSpell-Background.png";
import MKRLogo from "../../images/MKR-Logo.png";
import { getRatio } from "../../utils/helpers";

const DistribPanel = ({ totalSpellFees, MKRDistrib }) => {
  const formattedMKRDistrib = `${MKRDistrib}`.split(".")[0];
  const formattedSPELLDistrib = `${totalSpellFees}`.split(".")[0];
  const spellDistribToMakerRatio = getRatio(
    formattedSPELLDistrib,
    formattedMKRDistrib
  );

  return (
    <div className="Flip-panel-item">
      <div className="Flip-panel-text-container">
        <h4 className="Flip-panel-label">{"sSPELL fees last 7 days"}</h4>
        <img src={sSpellLogo} alt="Spell Logo" width={50} height={50} />
        <NumberFormat
          prefix="$"
          value={formattedSPELLDistrib}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
        <div data-tip="The abracadabra team redistributes rewards manually to prevent frontrunning.">
          <BsQuestionOctagonFill />
          <ReactTooltip />
        </div>
      </div>
      <ProgressBar
        value={
          Number.isNaN(spellDistribToMakerRatio)
            ? "0"
            : `${spellDistribToMakerRatio.toFixed(2)}`
        }
      />
      <div className="Flip-panel-text-container">
        <h4 className="Flip-panel-label">{"MKR fees last week (ETH only)"}</h4>
        <ReactTooltip />
        <img src={MKRLogo} alt="MakerDAO Logo" width={50} height={50} />
        <NumberFormat
          prefix="$"
          value={formattedMKRDistrib}
          displayType="text"
          thousandSeparator
          renderText={(value) => <p className="Flip-panel-label">{value}</p>}
        />
        <div data-tip="The burn mechanism of Maker only takes place when 60M worth of DAI have been accumulated. Ethereum only">
          <BsQuestionOctagonFill />
          <ReactTooltip />
        </div>
      </div>
    </div>
  );
};

DistribPanel.propTypes = {
  totalSpellFees: PropTypes.number,
  MKRDistrib: PropTypes.number,
};

export default DistribPanel;
