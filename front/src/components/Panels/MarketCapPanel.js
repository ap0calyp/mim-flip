import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import ProgressBar from "./../ProgressBar";
import SpellLogo from "../../images/Spell-Background-Large.png";
import MKRLogo from "../../images/MKR-Logo.png";
import { getRatio } from "../../utils/helpers";

const MarketCapPanel = ({ SPELLPrice, SPELLCap, MKRCap }) => {
  const spellToMakerRatio = getRatio(SPELLCap, MKRCap);
  const spellPriceForEqualMC = MKRCap * (SPELLPrice / SPELLCap);

  return (
    <div className="Flip-panel-mcap-container">
      <div className="Flip-panel-item">
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"SPELL Market cap"}</h4>
          <img src={SpellLogo} alt="Spell Logo" width={50} height={50} />
          <NumberFormat
            prefix="$"
            value={SPELLCap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
        <ProgressBar value={`${spellToMakerRatio.toFixed(2)}`} />
        <div className="Flip-panel-text-container">
          <h4 className="Flip-panel-label">{"MKR Market cap"}</h4>
          <img src={MKRLogo} alt="MakerDAO Logo" width={50} height={50} />
          <NumberFormat
            prefix="$"
            value={MKRCap}
            displayType="text"
            thousandSeparator
            renderText={(value) => <p className="Flip-panel-label">{value}</p>}
          />
        </div>
      </div>
      {SPELLPrice.toFixed(6) < spellPriceForEqualMC.toFixed(6) && (
        <>
          <p className="Flip-panel-future-price-container">
            {"At equal Market Cap, SPELL price would be"}
          </p>
          <p className="Flip-panel-future-price-container">
            <NumberFormat
              suffix="$"
              value={spellPriceForEqualMC.toFixed(6)}
              displayType="text"
              renderText={(value) => value}
            />
          </p>
        </>
      )}
    </div>
  );
};

MarketCapPanel.propTypes = {
  SPELLCap: PropTypes.number,
  MKRCap: PropTypes.number,
  SPELLPrice: PropTypes.number,
};

export default MarketCapPanel;
