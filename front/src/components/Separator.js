import PropTypes from "prop-types";
import colors from "../styles/colors";

const separatorStyle = {
  width: "80%",
  border: "1px solid",
  borderColor: colors.secondary,
  marginTop: "1%",
  marginBottom: "1%",
};

const lightSeparatorStyle = {
  width: "70%",
  border: "1px solid",
  borderColor: "gray",
  marginTop: "1%",
  marginBottom: "1%",
};

const Separator = ({ light }) => (
  <div style={light ? lightSeparatorStyle : separatorStyle} />
);

Separator.propTypes = {
  light: PropTypes.bool,
};

export default Separator;
