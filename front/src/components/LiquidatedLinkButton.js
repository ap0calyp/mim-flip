import "../styles/CommunityLinkButtons.css";

const LiquidatedLinkButton = () => (
  <div className="CommunityLinks-container">
    <h3>Community tools</h3>
    <div className="CommunityLinks-links-container">
      <h4 className="CommunityLinks-title">Find out if you got liquidated.</h4>
      <a
        className="CommunityLinks-link"
        href="https://liquidated.fyi/"
        target="_blank"
        rel="noreferrer"
      >
        Liquidated.fyi
      </a>
      <h4 className="CommunityLinks-title">Ser, wen Merlin candle ?</h4>
      <a
        className="CommunityLinks-link"
        href="https://wenmerl.in/"
        target="_blank"
        rel="noreferrer"
      >
        Wenmerl.in
      </a>
    </div>
  </div>
);

export default LiquidatedLinkButton;
