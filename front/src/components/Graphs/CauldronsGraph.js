import "../../styles/graphs/CauldronGraph.css";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import config from "../../config/config";
import {
  capitalizeFirstLetter,
  getChartWidthForSize,
  getNetworkIdFromName,
} from "../../utils/helpers";
import { getCauldronsMock } from "../../utils/getCauldronsMock";

const CauldronsGraph = ({ type }) => {
  const [cauldrons, setCauldrons] = useState(getCauldronsMock());
  const [error, setError] = useState("");
  let resizeCallToClear = null;
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth,
        }),
        100
      );
    }
    window.addEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV === "production") {
      fetchCauldrons();
    } else {
      if (type !== "all") {
        setCauldrons(
          cauldrons
            .filter(
              (cauldron) => cauldron.network === getNetworkIdFromName(type)
            )
            .sort(
              (cauldronA, cauldronB) =>
                parseFloat(cauldronA.fees) - parseFloat(cauldronB.fees)
            )
        );
      }
    }
  }, []);

  const fetchCauldrons = async () => {
    try {
      const responseSpellCauldrons = await fetch(
        `${config.backend}/getSpellCauldrons`
      );
      const responseSpellCauldronsJson = await responseSpellCauldrons.json();
      if (type === "all") {
        setCauldrons(
          responseSpellCauldronsJson.cauldrons.sort(
            (cauldronA, cauldronB) =>
              parseFloat(cauldronA.fees) - parseFloat(cauldronB.fees)
          )
        );
      } else {
        setCauldrons(
          responseSpellCauldronsJson.cauldrons
            .filter(
              (cauldron) => cauldron.network === getNetworkIdFromName(type)
            )
            .sort(
              (cauldronA, cauldronB) =>
                parseFloat(cauldronA.fees) - parseFloat(cauldronB.fees)
            )
        );
      }
    } catch (error) {
      setError(error.toString());
      console.log("error Cauldrons : ", error);
    }
  };

  if (error !== "") {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  if (cauldrons.length === 0) {
    return <div></div>;
  }

  const { width } = dimensions;

  const options = {
    legend: {
      show: true,
      position: "bottom",
      labels: {
        colors: ["white"],
      },
      fontSize: width < 650 ? "12px" : "17px",
      horizontalAlign: "center",
      width: `${width - 5}px`,
      inverseOrder: true,
    },
    tooltip: {
      fillSeriesColor: true,
      style: {
        fontSize: "18px",
      },
    },
    labels: [],
  };

  let series = [];
  cauldrons.forEach((cauldron) => {
    series.push(parseFloat(parseFloat(cauldron.fees).toFixed(3)));
    options.labels.push(cauldron.name);
  });
  let label = "across all chains";
  if (type !== "all") {
    label = `on ${capitalizeFirstLetter(type)}`;
  }

  return (
    <div className="Cauldron-Graph-container">
      <h3 className="Cauldron-Graph-title">
        {`Cauldrons fees earnings last 7 days ${label}`}
      </h3>
      <Chart
        options={options}
        series={series}
        type="donut"
        width={getChartWidthForSize(width)}
      />
    </div>
  );
};

CauldronsGraph.propTypes = {
  type: PropTypes.string,
};

export default CauldronsGraph;
