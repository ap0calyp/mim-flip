import "../../styles/graphs/PegGraph.css";
import moment from "moment-timezone";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import colors from "../../styles/colors";
import { getChartWidthForSize } from "../../utils/helpers";
import MyLoader from "../Loader";

export const PegGraph = () => {
  const [mimPrice, setMimPrice] = useState([]);
  const [ustPrice, setUstPrice] = useState([]);
  const [dayInterval, setDayInterval] = useState(30);
  const [isLoadingMimFeed, setIsLoadingMimFeed] = useState(false);
  const [isLoadingUstFeed, setIsLoadingUstFeed] = useState(false);
  const [width, setWidth] = useState(window.innerWidth);
  const [error, setError] = useState("");

  const timeInterval =
    (moment.tz("America/Costa_Rica").unix() -
      moment.tz("America/Costa_Rica").subtract(dayInterval, "d").unix()) /
    60;

  let resizeCallToClear = null;

  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(setWidth(window.innerWidth), 100);
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const fetchUstFeed = async () => {
    setIsLoadingUstFeed(true);
    fetch(
      `https://market.link/v1/metrics/api/v1/query_range?query=avg(feeds_latest_answer%7Bfeed_address%3D~%22(%3Fi)0x5edd5f803b831b47715ad3e11a90dd244f0cd0a9%22%2C%20network_id%3D~%221%7C%22%7D)%20by%20(feed_address)%20%2F%20100000000&start=${moment
        .tz("America/Costa_Rica")
        .subtract(dayInterval, "d")
        .unix()}&end=${moment
        .tz("America/Costa_Rica")
        .unix()}&step=${timeInterval}&days=${dayInterval}`
    )
      .then((response) => response.json())
      .then((data) => {
        setUstPrice(data.data.result[0].values);
        setIsLoadingUstFeed(false);
      })
      .catch((error) => {
        setIsLoadingUstFeed(false);
        setError(error.toString());
        console.log("Error UST feed : ", error);
      });
  };

  const fetchMimFeed = async () => {
    setIsLoadingMimFeed(true);
    fetch(
      `https://market.link/v1/metrics/api/v1/query_range?query=avg(feeds_latest_answer%7Bfeed_address%3D~%22(%3Fi)0x1a6E198c667223a4e1ecee7F5727E2A384210025%22%2C%20network_id%3D~%221%7C%22%7D)%20by%20(feed_address)%20%2F%20100000000&start=${moment
        .tz("America/Costa_Rica")
        .subtract(dayInterval, "d")
        .unix()}&end=${moment
        .tz("America/Costa_Rica")
        .unix()}&step=${timeInterval}&days=${dayInterval}`
    )
      .then((response) => response.json())
      .then((data) => {
        setMimPrice(data.data.result[0].values);
        setIsLoadingMimFeed(false);
      })
      .catch((error) => {
        setIsLoadingMimFeed(false);
        setError(error.toString());
        console.log("Error MIM feed : ", error);
      });
  };

  useEffect(() => {
    if (ustPrice.length === 0 && !isLoadingUstFeed) {
      fetchUstFeed();
    }
  });

  useEffect(() => {
    if (mimPrice.length === 0 && !isLoadingMimFeed) {
      fetchMimFeed();
    }
  });

  useEffect(() => {
    if (mimPrice.length > 0 && ustPrice.length > 0) {
      fetchUstFeed();
      fetchMimFeed();
    }
  }, [dayInterval]);

  if (
    mimPrice.length === 0 ||
    ustPrice.length === 0 ||
    isLoadingMimFeed ||
    isLoadingUstFeed
  ) {
    return (
      <div
        style={{
          minWidth: getChartWidthForSize(width),
          minHeight: "630.8px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <MyLoader />
      </div>
    );
  }

  if (error !== "") {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  const PegGraphOptions = {
    stroke: {
      show: true,
      curve: "smooth",
    },
    colors: [colors.secondary, "#ffffff", "#00ffb4"],
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      type: "datetime",
      categories: mimPrice.map((item) => item[0] * 1000),
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    yaxis: {
      labels: {
        style: {
          colors: "white",
        },
      },
      min: 0.99,
      max: 1.01,
      horizontalAlign: "center",
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm",
      },
      enabled: true,
      theme: "dark",
    },
    legend: {
      show: true,
      labels: {
        colors: ["white"],
      },
    },
  };

  const MIMSerie = {
    name: "MIM price feed",
    data: mimPrice.map((mim) => [mim[0] * 1000, mim[1]]),
  };

  const USTSerie = {
    name: "UST price feed",
    data: ustPrice.map((ust) => [ust[0] * 1000, ust[1]]),
  };

  const StableSerie = {
    name: "1$ mark",
    data: mimPrice.map((ust) => [ust[0] * 1000, 1.0]),
  };

  return (
    <div className="PegGraph-container">
      <h3>{`MIM and UST price feed over last ${
        dayInterval > 1 ? `${dayInterval} days` : "24hrs"
      }`}</h3>
      <p className="PegGraph-subtitle">{`Current UST price : ${
        ustPrice[ustPrice.length - 1][1]
      }`}</p>
      <p className="PegGraph-subtitle">{`Current MIM price : ${
        mimPrice[mimPrice.length - 1][1]
      }`}</p>
      <div className="PegGraph-buttons-container">
        <button
          style={dayInterval === 30 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(30)}
        >
          30 days
        </button>
        <button
          style={dayInterval === 7 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(7)}
        >
          7 days
        </button>
        <button
          style={dayInterval === 2 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(2)}
        >
          2 days
        </button>
        <button
          style={dayInterval === 1 ? { textDecoration: "underline" } : null}
          className="PegGraph-button"
          onClick={() => setDayInterval(1)}
        >
          24 hrs
        </button>
      </div>
      <Chart
        options={PegGraphOptions}
        series={[MIMSerie, USTSerie, StableSerie]}
        type="line"
        width={getChartWidthForSize(width)}
      />
      <p className="PegGraph-subtitle">{`Prices provided by Chainlink`}</p>
    </div>
  );
};
