import "../../styles/graphs/StakedRatioGraph.css";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import { getChartWidthForSize } from "../../utils/helpers";

const StakedRatioGraph = ({ historicalRatio }) => {
  let resizeCallToClear = null;
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });
  console.log("test : ", historicalRatio);
  useEffect(() => {
    function handleResize() {
      clearTimeout(resizeCallToClear);
      resizeCallToClear = setTimeout(
        setDimensions({
          height: window.innerHeight,
          width: window.innerWidth,
        }),
        100
      );
    }
    window.addEventListener("resize", handleResize);
  }, []);

  const options = {
    stroke: {
      show: true,
    },
    xaxis: {
      type: "datetime",
      labels: {
        style: {
          colors: "white",
        },
      },
    },
    yaxis: {
      type: "numeric",
      labels: {
        style: {
          colors: ["white"],
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: true,
      labels: {
        colors: ["white"],
      },
    },
    tooltip: {
      enabled: true,
      theme: "dark",
    },
  };

  let series = [
    {
      name: "SPELL for 1 sSPELL ",
      data: historicalRatio.map((ratioObject) => [
        `${ratioObject.timestamp}`.length === 16
          ? ratioObject.timestamp / 1000
          : ratioObject.timestamp,
        ratioObject.ratio.toFixed(10),
      ]),
    },
  ];

  const { width } = dimensions;

  return (
    <div className="StakedRatio-Graph-container">
      <h3 className="StakedSpell-Graph-title">
        {"Staked SPELL rewards ratio"}
      </h3>
      <p>
        Current staking ratio :{" "}
        {historicalRatio[historicalRatio.length - 1].ratio}
      </p>
      <Chart
        options={options}
        series={series}
        type="line"
        width={getChartWidthForSize(width)}
      />
    </div>
  );
};

StakedRatioGraph.propTypes = {
  historicalRatio: PropTypes.array,
};

export default StakedRatioGraph;
