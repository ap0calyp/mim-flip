import "react-day-picker/lib/style.css";
import "../styles/StakedRatioCalculator.css";
import DatePickerStyles from "../styles/DatePickerStyles.css";
import { useEffect, useState } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { formatDate, parseDate } from "react-day-picker/moment";
import config from "../config/config";
import { BsQuestionOctagonFill } from "react-icons/bs";
import ReactTooltip from "react-tooltip";

const StakedRatioCalculator = ({ historicalRatio }) => {
  const [dayRatio, setDayRatio] = useState(1.0);
  const [spellPrice, setSpellPrice] = useState(0.0);
  const [currentRatio, setCurrentRatio] = useState(1.0);
  const [spellAmount, setSpellAmount] = useState(0.0);
  const [dateError, setDateError] = useState("");
  const [error, setError] = useState("");
  const [day, setDay] = useState(null);

  const fetchSpellPrice = async () => {
    try {
      const responseSPELL = await fetch(
        `${config.coingeckoApiURL}/coins/spell-token`
      );
      const dataSPELL = await responseSPELL.json();
      setSpellPrice(dataSPELL.market_data.current_price.usd);
    } catch (error) {
      setError(error.toString());
    }
  };

  const handleDayChange = (day) => {
    if (moment(day).isSameOrAfter(moment(), "day")) {
      setDateError("Cannot read the future.");
      return;
    } else if (moment(day).isBefore(moment("2021/05/30 21:00:00"), "day")) {
      setDateError("Too far in the past.");
      return;
    } else {
      setDay(day);
      setDateError("");
    }
  };
  const handleSpellAmountChange = (evt) => setSpellAmount(evt.target.value);

  useEffect(() => {
    fetchSpellPrice();
    setCurrentRatio(historicalRatio[historicalRatio.length - 1].ratio);
  }, []);

  useEffect(() => {
    if (day !== null) {
      const foundRatioObject = historicalRatio.find((ratioObject) =>
        moment(ratioObject.timestamp).isSame(day, "day")
      );
      setDayRatio(foundRatioObject.ratio);
    }
  }, [day]);

  let twoMonthsAgo = new Date();
  twoMonthsAgo.setMonth(twoMonthsAgo.getMonth() - 2);

  if (error !== "") {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  return (
    <div className="StakedRatio-Calculator-container">
      <h3 className="StakedRatio-Calculator-title">
        {"Staked SPELL rewards calculator"}
      </h3>

      <div className="StakedRatio-Calculator-Inputs-container">
        <div className="StakedRatio-Calculator-Input-container">
          <label className="StakedRatio-Calculator-Input-label">
            Amount staked
          </label>
          <input
            className="StakedRatio-Calculator-input"
            type="number"
            step="0.1"
            min="0"
            value={spellAmount}
            onChange={handleSpellAmountChange}
          />
        </div>
        <div className="StakedRatio-Calculator-Input-container">
          <label className="StakedRatio-Calculator-Input-label">
            Staking date
          </label>
          <DayPickerInput
            formatDate={formatDate}
            parseDate={parseDate}
            placeholder={`${formatDate(new Date())}`}
            classNames={DatePickerStyles}
            onDayChange={handleDayChange}
            disabledDays={{
              before: new Date("May-30-2021 09:00:00 PM"),
              after: moment().day,
            }}
          />
          {dateError !== "" && <p>{dateError}</p>}
        </div>
      </div>
      {day !== null && (
        <div className="StakedRatio-Calculator-Text-container">
          <p className="StakedRatio-Calculator-text">Staking</p>
          <p className="StakedRatio-Calculator-text">
            <NumberFormat
              suffix=" SPELL"
              value={spellAmount}
              displayType="text"
              thousandSeparator
              className="StakedRatio-Calculator-number"
            />
          </p>
          <p className="StakedRatio-Calculator-text">
            since {moment(day).format("MM-DD-YYYY")}
          </p>
          <p className="StakedRatio-Calculator-text">earned you</p>
          <div className="StakedRatio-Calculator-text">
            <NumberFormat
              prefix="~"
              suffix={` SPELL (~${(
                spellPrice *
                (currentRatio * spellAmount - dayRatio * spellAmount).toFixed(2)
              ).toFixed(2)}$)`}
              className="StakedRatio-Calculator-number"
              value={(
                currentRatio * spellAmount -
                dayRatio * spellAmount
              ).toFixed(2)}
              displayType="text"
              thousandSeparator
            />
            <div data-tip="Add a grain of salt to the mixture :)">
              <BsQuestionOctagonFill />
              <ReactTooltip />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

StakedRatioCalculator.propTypes = {
  historicalRatio: PropTypes.array,
};

export default StakedRatioCalculator;
