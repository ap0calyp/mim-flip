import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import colors from "../styles/colors";

const MyLoader = () => (
  <Loader type="Puff" color={colors.secondary} height={100} width={100} />
);

export default MyLoader;
