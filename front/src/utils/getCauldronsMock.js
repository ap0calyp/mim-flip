export const getCauldronsMock = () => {
  return [
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 1",
      fees: 4000.12341342,
      network: 1,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 2",
      fees: 8000.12341342,
      network: 1,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 3",
      fees: 6000.12341342,
      network: 56,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 4",
      fees: 6240.12341342,
      network: 56,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 5",
      fees: 10000.12341342,
      network: 56,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 6",
      fees: 10000.12341342,
      network: 250,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 7",
      fees: 10022.12341342,
      network: 250,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 8",
      fees: 221024.12341342,
      network: 250,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 9",
      fees: 10000.12341342,
      network: 250,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 10",
      fees: 2520.12341342,
      network: 42161,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 11",
      fees: 8429.12341342,
      network: 42161,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 12",
      fees: 4444.12341342,
      network: 42161,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 13",
      fees: 10000.12341342,
      network: 43114,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 14",
      fees: 8888.12341342,
      network: 43114,
    },
    {
      address: "0xSQDKJQSDQSDQSd",
      name: "Test 15",
      fees: 7777.12341342,
      network: 43114,
    },
  ].sort(
    (cauldronA, cauldronB) =>
      parseFloat(cauldronA.fees) - parseFloat(cauldronB.fees)
  );
};
