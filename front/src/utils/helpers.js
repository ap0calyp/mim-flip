export const getRatio = (val1, val2) =>
  (parseInt(val1, 10) / parseInt(val2, 10)) * 100;

export const getGraphSizeForResponsive = (width) => {
  if (width <= 360) {
    return 250;
  }
  if (width > 360 && width < 430) {
    return 280;
  }
  if (width > 360 && width < 650) {
    return 300;
  }
  if (width >= 650) {
    return 450;
  }
};

export const getChartWidthForSize = (width) => {
  if (width >= 610) {
    return "550px";
  }
  if (width < 610 && width > 510) {
    return "500px";
  }
  if (width < 510 && width >= 390) {
    return "400px";
  }
  if (width < 390 && width > 320) {
    return "330px";
  }
  return "300px";
};

export const getNetworkIdFromName = (name) => {
  switch (name) {
    default:
    case "ethereum":
      return 1;
    case "binance":
      return 56;
    case "fantom":
      return 250;
    case "arbitrum":
      return 42161;
    case "avalanche":
      return 43114;
  }
};

export const capitalizeFirstLetter = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1);
