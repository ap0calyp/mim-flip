import "../styles/About.css";
import Clonescody from "../images/Clonescody.png";
import Neuro from "../images/Neuro.png";
import Melen from "../images/Melen.png";
import Apocalyp from "../images/Apocalyp.jpeg";
import Roxxer from "../images/Roxxer.jpeg";
import Alexandomega from "../images/Alexandomega.jpeg";

const About = () => (
  <div className="About-container">
    <h3>Abracadabra</h3>
    <p>
      Abracadabra is a protocol that allow users to deposit interest bearing
      assets (ibAssets), and to borrow the MIM stablecoin (Magic Internet Money)
      against it.
    </p>
    <p>
      The protocol is based on the Kashi suite of Smart Contract built by
      BoringCrypto. The Kashi system allows to create independent lending
      markets. Those lending markets are reffered as Cauldrons on the
      Abracadabra protocol.
    </p>
    <p>
      Therefore, each cauldron on Abracadabra has a specific amount of MIMs that
      can be borrowed from it, segmenting the risk.
    </p>
    <p>
      The protocol also introduced a looping system, where users can deposit
      their ibAssets and let the system borrow MIMs, buy more of their
      collateral, deposit it again, and repeat as many times the user wants,
      hence introducing Leveraged Yield Farming.
    </p>
    <h3>ByeByeDai</h3>
    <p>
      With most of the current stablecoins being centralized, DAI was firstly
      seen as a safe heaven for most of DeFi users. However, almost half of the
      DAI supply is now backed by USDC deposit, making it vulnerable to
      censorship.
    </p>
    <p>
      As the MIM&apos;s main goal is to remain decentralized and
      censorship-resistant, we built this website to compare MakerDAO, the
      protocol minting DAI, and the MIMs created by Abracadabra.
    </p>
    <p>
      We then expanded the purpose of this site to be a dashboard which monitors
      many aspects of the Abracadabra protocol.
    </p>
    <h3>Builders</h3>
    <p>
      This website has been built by three french community wizards, not
      affiliated with the Abracadabra team.
    </p>
    <div className="About-builders-container">
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Melen twitter image"
          src={Melen}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/MelenXYZ"
          rel="noreferrer"
        >
          Mélen
        </a>
      </div>
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Neuro twitter image"
          src={Neuro}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/DefiNeuro"
          rel="noreferrer"
        >
          Neuro
        </a>
      </div>
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Clonescody twitter image"
          src={Clonescody}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/Clonescody"
          rel="noreferrer"
        >
          Clonescody
        </a>
      </div>
    </div>
    <h3>Contributors</h3>
    <p>
      Thanks to the other community members building awesome tools and helping
      all the frogs !
    </p>
    <div className="About-builders-container">
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Apocalyp twitter image"
          src={Apocalyp}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/ap0calyp1"
          rel="noreferrer"
        >
          Apocalyp
        </a>
        <p className="App-link-subtitle">
          For building the liquidation adaptator and website.
        </p>
      </div>
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Roxxer twitter image"
          src={Roxxer}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/r0xx3r"
          rel="noreferrer"
        >
          r0xx3r
        </a>
        <p className="App-link-subtitle">
          For the Wenmerlin website tracking the buybacks.
        </p>
      </div>
      <div className="About-builders-item">
        <img
          className="About-builders-image"
          alt="Alex and omega twitter image"
          src={Alexandomega}
        />
        <a
          className="App-link"
          target="_blank"
          href="https://twitter.com/alexandomega"
          rel="noreferrer"
        >
          {"Alex & omega"}
        </a>
        <p className="App-link-subtitle">For the borrowable MIMs dashboard.</p>
      </div>
    </div>
    <h3>Datas</h3>
    <p>This website uses datas provided by : </p>
    <div>
      <a
        className="App-link"
        href="https://www.coingecko.com/en/api"
        target="_blank"
        rel="noreferrer"
      >
        {"> The Coingecko API"}
      </a>
    </div>
    <div>
      <a
        className="App-link"
        href="https://cryptofees.info/api-docs"
        target="_blank"
        rel="noreferrer"
      >
        {"> The CryptoFees API"}
      </a>
    </div>
    <div>
      <a
        className="App-link"
        href="https://market.link/feeds/5ac223ca-12f9-498e-b394-84c86ce40a62"
        target="_blank"
        rel="noreferrer"
      >
        {"> The UST/USD Chainlink price feed"}
      </a>
    </div>
    <div>
      <a
        className="App-link"
        href="https://market.link/feeds/8404bc7b-dbb6-4bfa-a204-6912404b13d8"
        target="_blank"
        rel="noreferrer"
      >
        {"> The MIM/USD Chainlink price feed"}
      </a>
    </div>
    <h3>Links</h3>
    <div>
      <a
        className="App-link"
        href="https://abracadabra.money/"
        target="_blank"
        rel="noreferrer"
      >
        {"> Start making some magic at Abracadabra"}
      </a>
    </div>
    <div>
      <a
        className="App-link"
        href="https://discord.gg/mim"
        target="_blank"
        rel="noreferrer"
      >
        {"> Join the wizards academy on Discord"}
      </a>
    </div>
    <div>
      <a
        className="App-link"
        href="https://gitlab.com/Clonescody/mim-flip"
        target="_blank"
        rel="noreferrer"
      >
        {"> ByeByeDai repository"}
      </a>
    </div>
    <h3>Support</h3>
    <p>
      If you like the website, want to help us maintain it or just want to thank
      us, you can tip our multisig on multiple networks.
    </p>
    <p>Note that any DAI, USDT or USDC sent to us will be swapped to MIM :)</p>
    <p>
      {"Donation address on Ethereum Network : "}
      <a
        className="App-link"
        href="https://etherscan.io/address/0x54763E5c753cB463F2EF1d918B79592B41f9c93A"
        target="_blank"
        rel="noreferrer"
      >
        0x54763E5c753cB463F2EF1d918B79592B41f9c93A
      </a>
    </p>
    <p>
      {"Donation address on Arbitrum Network : "}
      <a
        className="App-link"
        href="https://arbiscan.io/address/0x59A92DE977F1401169416BAFB083808D0756Ee43"
        target="_blank"
        rel="noreferrer"
      >
        0x59A92DE977F1401169416BAFB083808D0756Ee43
      </a>
    </p>
    <p>
      {"Donation address on Fantom Network : "}
      <a
        className="App-link"
        href="https://ftmscan.com/address/0xDfEDc7bff79268D99F9E8Ae658A62b8611606b09"
        target="_blank"
        rel="noreferrer"
      >
        0xDfEDc7bff79268D99F9E8Ae658A62b8611606b09
      </a>
    </p>
    <p>
      {"Donation address on Avalanche Network : "}
      <a
        className="App-link"
        href="https://snowtrace.io/address/0x293f2ba866e11cf8359ccd3fcf46784648de364e"
        target="_blank"
        rel="noreferrer"
      >
        0x293f2BA866e11Cf8359Ccd3Fcf46784648dE364E
      </a>
    </p>
  </div>
);

export default About;
