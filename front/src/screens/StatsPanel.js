import "../styles/StatsPanel.css";
import { useEffect, useState } from "react";
import config from "../config/config";
import MyLoader from "../components/Loader";
import CauldronsGraph from "../components/Graphs/CauldronsGraph";
// import HoldersGraph from "../components/Graphs/HoldersGraph";
import StakedRatioGraph from "../components/Graphs/StakedRatioGraph";
import StakedRatioCalculator from "../components/StakedRatioCalculator";
import { getHistoricalRatioMock } from "../utils/getHistoricalRatioMock";
import { PegGraph } from "../components/Graphs/PegGraph";
import LiquidatedLinkButton from "../components/LiquidatedLinkButton";

const historicalRatioModel = getHistoricalRatioMock();

const StatsPanel = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [historicalRatio, setHistoricalRatio] = useState(historicalRatioModel);
  const [error, setError] = useState(null);

  const fetchInfos = async () => {
    setIsLoading(true);
    try {
      const responseHistoricalRatio = await fetch(
        `${config.backend}/getHistoricalStakedSpellRatio`
      );
      const historicalRatioJSON = await responseHistoricalRatio.json();
      setHistoricalRatio(historicalRatioJSON.stakedRatioHistory);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      setError(`${e}`);
      console.log("error stats panel : ", e);
    }
  };

  useEffect(() => {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV === "production") {
      fetchInfos();
    }
  }, []);

  if (error !== null) {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  return isLoading ? (
    <MyLoader />
  ) : (
    <div className="Stats-panel-container">
      <div className="Stats-panel-row">
        <StakedRatioGraph historicalRatio={historicalRatio} />
        <PegGraph />
      </div>
      <div className="Stats-panel-row">
        <LiquidatedLinkButton />
        <StakedRatioCalculator historicalRatio={historicalRatio} />
      </div>
      <div className="Stats-panel-row">
        <CauldronsGraph type="all" />
      </div>
      <div className="Stats-panel-row">
        <CauldronsGraph type="ethereum" />
        <CauldronsGraph type="avalanche" />
      </div>
      <div className="Stats-panel-row">
        <CauldronsGraph type="fantom" />
        <CauldronsGraph type="arbitrum" />
      </div>
    </div>
  );
};

export default StatsPanel;
