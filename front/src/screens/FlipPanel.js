import "../styles/FlipPanel.css";
import { useEffect, useState } from "react";
import Separator from "../components/Separator";
import MyLoader from "../components/Loader";
import TVLPanel from "../components/Panels/TVLPanel";
import SupplyPanel from "../components/Panels/SupplyPanel";
import MarketCapPanel from "../components/Panels/MarketCapPanel";
import config from "../config/config";
import DistribPanel from "../components/Panels/DistribPanel";
import EmissionsPanel from "../components/Panels/RevenuesPanel";

const flipInfosModel = {
  spellTVL: 0,
  mkrTVL: 0,
  mimCap: 0,
  daiCap: 0,
  usdcCap: 0,
  usdtCap: 0,
  spellCap: 0,
  mkrCap: 0,
  totalSpellFees: 0,
  mkrDistrib: 0,
  spellPrice: 0.0,
  emissionsUSD: 0.0,
};

const FlipPanel = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [flipInfos, setFlipInfos] = useState(flipInfosModel);
  const [error, setError] = useState(null);

  const fetchInfos = async () => {
    try {
      const responseSPELL = await fetch(
        `${config.coingeckoApiURL}/coins/spell-token`
      );
      const dataSPELL = await responseSPELL.json();

      const responseMKR = await fetch(`${config.coingeckoApiURL}/coins/maker`);
      const dataMKR = await responseMKR.json();

      const responseDAI = await fetch(`${config.coingeckoApiURL}/coins/dai`);
      const dataDAI = await responseDAI.json();

      const responseUSDC = await fetch(
        `${config.coingeckoApiURL}/coins/usd-coin`
      );
      const dataUSDC = await responseUSDC.json();

      const responseUSDT = await fetch(
        `${config.coingeckoApiURL}/coins/tether`
      );
      const dataUSDT = await responseUSDT.json();

      const responseMIM = await fetch(
        `${config.coingeckoApiURL}/coins/magic-internet-money`
      );
      const dataMIM = await responseMIM.json();
      let makerFees = 100.0;
      let responseSpellFeesJson = 2000000;
      let emissionsUSD = 10000;
      // eslint-disable-next-line no-undef
      if (process.env.NODE_ENV === "production") {
        const responseCryptoFees = await fetch(`${config.backend}/getMkrFees`);
        makerFees = await responseCryptoFees.json();

        const responseSpellFees = await fetch(`${config.backend}/getSpellFees`);
        responseSpellFeesJson = await responseSpellFees.json();

        const responseEmissions = await fetch(`${config.backend}/getEmissions`);
        emissionsUSD = await responseEmissions.json();
      }

      setFlipInfos({
        ...flipInfos,
        spellTVL: dataSPELL.market_data.total_value_locked.usd,
        mkrTVL: dataMKR.market_data.total_value_locked.usd,
        daiCap: dataDAI.market_data.circulating_supply,
        mimCap: dataMIM.market_data.circulating_supply,
        usdcCap: dataUSDC.market_data.circulating_supply,
        usdtCap: dataUSDT.market_data.circulating_supply,
        spellCap: dataSPELL.market_data.market_cap.usd,
        mkrCap: dataMKR.market_data.market_cap.usd,
        totalSpellFees: responseSpellFeesJson,
        mkrDistrib: makerFees,
        spellPrice: dataSPELL.market_data.current_price.usd,
        emissionsUSD: parseInt(emissionsUSD),
      });
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      setError(`${e}`);
    }
  };

  useEffect(() => {
    fetchInfos();
  }, []);

  if (error !== null) {
    return (
      <div className="App-flip-panel">
        <p>{error}</p>
      </div>
    );
  }

  const {
    spellTVL,
    mkrTVL,
    mimCap,
    daiCap,
    usdcCap,
    usdtCap,
    spellCap,
    mkrCap,
    totalSpellFees,
    mkrDistrib,
    spellPrice,
    emissionsUSD,
  } = flipInfos;

  return (
    <div className="App-flip-panel">
      {isLoading ? (
        <MyLoader />
      ) : (
        <>
          {/* <h4 className="App-disclaimer">
            {"Disclaimer : datas for earned fees aren't accurate, use the "}
            <a
              className="App-link"
              href="https://app.powerbi.com/view?r=eyJrIjoiOWExYTc5ZDYtN2IyZS00YmMyLTgyNzAtYzMxOTM4YmE2MGJlIiwidCI6IjYyZTU1MTgwLTQzNmQtNDYyZC1hMWIwLTZkMTg2NjRlZDAxNSJ9"
              target="_blank"
              rel="noreferrer"
            >
              {"official dashboard"}
            </a>
            {" as reference while we work on improvements."}
          </h4> */}
          <TVLPanel SPELLTvl={spellTVL} MKRTvl={mkrTVL} />
          <Separator light={false} />
          <DistribPanel
            totalSpellFees={totalSpellFees}
            MKRDistrib={mkrDistrib}
          />
          <Separator light={true} />
          <EmissionsPanel
            emissionsUSD={emissionsUSD}
            revenuesUSD={parseInt(totalSpellFees)}
          />
          <Separator light={true} />
          <MarketCapPanel
            SPELLPrice={spellPrice}
            SPELLCap={spellCap}
            MKRCap={mkrCap}
          />
          <Separator light={true} />
          <SupplyPanel
            USDCCap={usdcCap}
            USDTCap={usdtCap}
            DAICap={daiCap}
            MIMCap={mimCap}
          />
        </>
      )}
    </div>
  );
};

export default FlipPanel;
