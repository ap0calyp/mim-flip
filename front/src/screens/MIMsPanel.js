import "../styles/MIMsPanel.css";
import { useEffect, useState } from "react";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

import MyLoader from "../components/Loader";
import BorrowableMIMsPanel from "../components/Panels/BorrowableMIMsPanel";
import config from "../config/config";
import colors from "../styles/colors";

const MIMsPanel = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [borrowables, setBorrowables] = useState([]);

  const fetchBorrowableMims = async () => {
    console.log("Refresh");
    const responseBorrowableMims = await fetch(
      `${config.backend}/getBorrowableMims`
    );
    const borrowablesMimsJSON = await responseBorrowableMims.json();
    setBorrowables(borrowablesMimsJSON.cauldrons);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchBorrowableMims();
  }, []);

  if (isLoading) {
    return (
      <div className="MIM-Loading-container">
        <MyLoader />
      </div>
    );
  }

  return (
    <div className="MIM-Container">
      <h2>Amount of MIMs available for borrowing</h2>
      <div className="MIM-Countdown-container">
        <p className="MIM-Countdown-title">Refreshes in</p>
        <CountdownCircleTimer
          isPlaying
          duration={5}
          colors={[colors.primaryLight, colors.secondary]}
          colorsTime={[5, 0]}
          size={75}
          onComplete={() => {
            fetchBorrowableMims();
            return { shouldRepeat: true, delay: 2 };
          }}
        >
          {({ remainingTime }) => remainingTime}
        </CountdownCircleTimer>
      </div>
      <div className="MIM-Borrowable-Container">
        <div className="MIM-Container-col">
          <BorrowableMIMsPanel
            chain={borrowables[0].chain}
            cauldrons={borrowables[0].cauldrons}
          />
        </div>
        <div className="MIM-Container-col">
          <BorrowableMIMsPanel
            chain={borrowables[1].chain}
            cauldrons={borrowables[1].cauldrons}
          />
          <BorrowableMIMsPanel
            chain={borrowables[3].chain}
            cauldrons={borrowables[3].cauldrons}
          />
        </div>
        <div className="MIM-Container-col">
          <BorrowableMIMsPanel
            chain={borrowables[4].chain}
            cauldrons={borrowables[4].cauldrons}
          />
          <BorrowableMIMsPanel
            chain={borrowables[2].chain}
            cauldrons={borrowables[2].cauldrons}
          />
        </div>
      </div>
    </div>
  );
};

export default MIMsPanel;
