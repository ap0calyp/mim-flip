import "./styles/App.css";
import { Route, Routes } from "react-router";
import Header from "./components/Navigation/Header";
import Footer from "./components/Navigation/Footer";
import FlipPanel from "./screens/FlipPanel";
import StatsPanel from "./screens/StatsPanel";
import About from "./screens/About";
import MIMsPanel from "./screens/MIMsPanel";

const App = () => {
  return (
    <div className="App">
      <Header />
      <div className="App-container">
        <Routes>
          <Route path="/" element={<FlipPanel />} />
          <Route path="stats" element={<StatsPanel />} />
          <Route path="mim" element={<MIMsPanel />} />
          <Route path="about" element={<About />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
};

export default App;
